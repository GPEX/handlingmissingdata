package fr.lirmm.GPMV;

import java.util.List;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;

public class CypherQueries {

	public static long countN(Transaction tx) {
		String COUNT_NODES = ("MATCH (a) RETURN count(a)");
		StatementResult result = tx.run(COUNT_NODES);
		return result.single().get(0).asLong();

	}

	public static long countR(Transaction tx) {
		String COUNT_REL = ("MATCH ()-[r]->()  RETURN count(*)");
		StatementResult result = tx.run(COUNT_REL);
		return result.single().get(0).asLong();

	}

	public static List<Record> getDB(Transaction tx) {
		String GET_DB = ("MATCH (n)\r\n"
				+ "RETURN head(labels(n)) as label, keys(n) as properties, count(*) as count\r\n"
				+ "ORDER BY count DESC");

		StatementResult result = tx.run(GET_DB);
		List<Record> records = result.list();

		return records;

	}

}
