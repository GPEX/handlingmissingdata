package lirmm.fr;

//package Generator;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

public class GradualPoly 
{
	public String fileOut;
	
	public int nbClient;
	public int nbItem;
//	public int missing;
	public int ign = -1;
	public int nbVal;
	
	public Vector <Integer> coeff;
	public Vector <Integer> [] abs;
	public Vector <Integer> previous;
	
	@SuppressWarnings("unchecked")
	public GradualPoly (String fo, int cl, int it, int val)
	{
		fileOut = fo;
		
		nbClient = cl;
		nbItem = it;
		nbVal =val;
		
		//missing = new Double (m * it).intValue();
		coeff = new Vector <Integer> ();
		RemplirCoeff();
		previous = new Vector <Integer> ();
		
		for (int i = 0; i < nbItem; i++)
			previous.add(0);
		
		//AfficherVector(coeff);
		
		abs = new Vector [nbItem];
		
		Double tmp = new Double(100.0 / nbItem);
		for (int i = 0; i < nbItem; i++)
		{
			// calculer combien d'abscence
			int pourcent = nbClient - new Double((((nbItem - i) * tmp) / 100) * nbClient).intValue();
			//System.out.println("traitement de l'item "+(i+1) + " presence a " + pourcent);
			remplirAbs(pourcent, i);
			//System.out.print("i = "+i + " ");
			//AfficherVector(abs[i]);
		}
	}
	
	public void RemplirCoeff ()
	{
		Random generator = new Random();
		//
		for (int i = 0; i < nbItem; i++)
		{
			int n = (generator.nextInt(10)+1)%10;
			boolean signe = generator.nextBoolean();
			coeff.add(new Integer(n)*(signe ? 1 : -1));
		}
	}
	
	/**
	 * Remplir les lignes où l'item ne sera pas présent.
	 * @param it : l'item à remplir
	 * @param nb : le nombre de lignes à mettre dedans
	 **/
	public void remplirAbs (int nb, int it)
	{
		Random generator = new Random();
		abs[it] = new Vector<Integer> ();
		int i = 0;
		//System.out.println("nb = "+nb);
		while (i < nb)
		{
			int n = (generator.nextInt(nbClient)+1)%nbClient;
			//System.out.println("Traite indice "+ i + " n = "+n);
			if (!abs[it].contains(new Integer(n)))
			{
				abs[it].add(new Integer(n));
				i++;
			}
		}
		Collections.sort(abs[it]);
	}
	
	public void Generate ()
	{
		try 
		{
			FileWriter fw = new FileWriter (fileOut);
			Random generator = new Random();
			// Ecrire la première ligne
			String s = "";
			for (int i = 0; i < nbItem; i++)
				s = s + (i == 0 ? "" : " ") + (i+1);
			fw.write(s+"\n");
			
			for (int i = 0; i < nbClient; i++)
			{
				s = "";
				for (int j = 0; j < nbItem; j++)
				{
					if (abs[j].contains(i))
						s = s + (j == 0 ? "" : " ")+ "-1";
					else // l'item est présent
					{
						//int value = coeff.get(j).intValue() * j;
						int value = (previous.get(j) == 0 ? (generator.nextInt(nbVal)+1)%nbVal : previous.get(j) * Math.abs(coeff.get(j)));
						if (previous.get(j) == 0)
							previous.set(j, new Integer(1));
						else
							previous.set(j, previous.get(j)+1);
						
						s = s + (j == 0 ? "" : " ")+ (value * (coeff.get(j) < 0 ? -1 : 1));
					}
				}
				fw.write(s+"\n");
			}
			fw.close();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void AfficherVector (Vector <Integer> v)
	{
		System.out.print("[");
		for (int i = 0; i < v.size(); i++)
			System.out.print((i == 0 ? "" : ", ") + v.get(i));
		System.out.println("]");
	}
	public static void main(String[] args) 
	{
		if (args.length < 4)  
		{
			System.out.println("Utilisation : GradualPoly <fileout> <nbTrans> <nbItems> <nbVals>");
			return;
		}
		else
		{
			GradualPoly gp = new GradualPoly (args[0], new Integer (args[1]).intValue(), new Integer (args[2]).intValue(), new Integer (args[3]).intValue());
			gp.Generate();
		}
	}
}
