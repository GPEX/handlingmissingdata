# R script to impute missing data in dataset

# store the current directory
initial.dir<-getwd()

# change to the new directory
setwd("/home/faaiz/git/GPMVI/GPMVI")

# load the necessary libraries
require(Amelia)

# load the dataset
library(readr)
User <- read_csv("User.csv", col_types = cols(`[statusesCount` = col_number(), 
        favouritesCount = col_number(), followersCount = col_number(), 
        friendsCount = col_number(), `listedCount]` = col_number(), 
        timeZone = col_number(), userId = col_number()))


# Impute using Amelia
User.out <- amelia(User, m= 1, boot.type = "none")

User.out
# Write to CSV file 
write.amelia(obj = User.out, file.stem = "User")

# Create Missing data map
missmap(User.out) 

# unload the libraries
detach("package:Amelia")

# change back to the original directory
setwd(initial.dir)
