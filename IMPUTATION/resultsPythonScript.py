#!/usr/bin/python
import os
import sys, getopt
import time
import csv
import argparse
from fnmatch import fnmatch

 
parser = argparse.ArgumentParser(description='This is a script gradual pattern extraction program.')
parser.add_argument('-i','--input', help='Input file name',required=True)
parser.add_argument('-s','--minSupport',help='Minimum support value', required=True)
args = parser.parse_args()

## show values ##
## show values ##
print ("\n WELCOME TO GRADUAL PATTERN EXTRACTION PROGRAM \n" )
print ("Input file: %s" % args.input )
print ("Minimum support: %s" % args.minSupport+"\n" ) 

#inFile = sys.argv[1]
#support = sys.argv[2]
inFile = args.input
support = args.minSupport
path = os.getcwd()
for file in os.listdir(path):
	if fnmatch(file,inFile):
		name = path + '/results/' 
		file2 = open(name +'/'+ file[:-4] + '.csv', 'wb')
		l = []
		l1 = []
		writer = csv.writer(file2)
		for x in range(0,5):
			os.system('java  -Xmx2g -jar '+ file +' '+ support +'  >' + name +'/'+ file[:-4] +'.'+ str(x) +'.txt')
			time.sleep(5)
			handle = open(name +'/'+ file[:-4] +'.'+ str(x) +'.txt', 'r')
			lines_list = handle.readlines()
			last = lines_list[-1]
			sec_last = lines_list[-2]
			mem = sec_last[27:-1]
			sec = last[19:-8]
			#print "time: "+sec+ " Mem: "+ mem 
			writer.writerow([sec,mem])
			l.append(float(sec))
			l1.append(float(mem))
		writer.writerow(["AvgTime:", "AvgMem:"])
		avg = sum(l)/float(len(l))
		avg2 = sum(l1)/float(len(l1))
		print "AvgTimeConsumption: " + str(avg) + "   AvgMemoryUtilization: "+ str(avg2)+"\n"
		writer.writerow([avg, avg2])
		




