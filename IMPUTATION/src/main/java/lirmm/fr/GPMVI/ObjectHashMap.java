package lirmm.fr.GPMVI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


/*
////////////HashMap for Hepatitis DataSet ////////////////////////////

public class ObjectHashMap {

	Map<String, Boolean> hepatitis = new HashMap<String, Boolean>();

	ObjectHashMap() { // Synthetic dataset

		hepatitis.put("Age", true);
		hepatitis.put("BILIRUBIN", true);
		hepatitis.put("ALKphosphate", true);
		hepatitis.put("SGOT", true);
		hepatitis.put("ALBUMIN", true);
		hepatitis.put("PROTIME", true);

	}

	public List<String> getSortablePropertiesList(String mapName) {

		List<String> propertyList = new ArrayList<String>();

		if (mapName.equalsIgnoreCase("hepatitis")) {
			Set<Entry<String, Boolean>> keySet = hepatitis.entrySet();

			for (Entry<String, Boolean> val : keySet) {
				if ((boolean) val.getValue()) {
					propertyList.add(val.getKey());

				}
			}
		}
		return propertyList;
	}

/////////////////////////////////////////////////////////////////////////
	
*/

//////////// HashMap for Synthetic DataSet ////////////////////////////


public class ObjectHashMap {


	Map<String, Boolean> Synthetic = new HashMap<String, Boolean>();

	 ObjectHashMap() {  // Synthetic dataset 
		 

	//	 Synthetic.put("Id", true);
		 Synthetic.put("A99RO", true);
		 Synthetic.put("B99RO", true);
		 Synthetic.put("C99RO", true);
		 Synthetic.put("D99RO", true);
		 Synthetic.put("E99RO", true);
	


	}

	public List<String> getSortablePropertiesList(String mapName) {

		List<String> propertyList = new ArrayList<String>();

		if (mapName.equalsIgnoreCase("synthetic")) {
			Set<Entry<String, Boolean>> keySet = Synthetic.entrySet();

			for (Entry<String, Boolean> val : keySet) {
				if ((boolean) val.getValue()) {
					propertyList.add(val.getKey());

				}
			}
		} 
		return propertyList;
	}

	
//////////////////////////////////////////////////////////////////////


/*

//////////////// HashMap for Russian tweet troll Dataset//////////////

	
public class ObjectHashMap {

     Map<String, Boolean> User = new HashMap<String, Boolean>();


	 ObjectHashMap() {  // Russian_Tweet_Troll dataset 

		User.put("userId", true);
		User.put("followersCount", true);
		User.put("statusesCount", true);
		User.put("timeZone", true);
		User.put("favouritesCount", true);
		User.put("friendsCount", true);
		User.put("listedCount", true);


	}

	public List<String> getSortablePropertiesList(String mapName) {

		List<String> propertyList = new ArrayList<String>();

		if (mapName.equalsIgnoreCase("user")) {
			Set<Entry<String, Boolean>> keySet = User.entrySet();

			for (Entry<String, Boolean> val : keySet) {
				if ((boolean) val.getValue()) {
					propertyList.add(val.getKey());

				}
			}
		} 
		return propertyList;
	}
	
////////////////////////////////////////////////////////
*/
	
}
