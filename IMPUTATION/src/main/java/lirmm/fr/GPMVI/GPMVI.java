package lirmm.fr.GPMVI;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;

public class GPMVI {
	public static void main(String[] args) throws IOException {
			
		double support = Double.parseDouble(args[0]);   // support input parameter
//		String csvFile =  args[1];
			
		long start_time = 0, end_time = 0, elapsedTime = 0;
		start_time = System.nanoTime();

		long beforeUsedMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long megaByte = 1024L * 1024L;


//		String csvFile = "/auto/shah/javaCode/csv/toyExample.csv"; // path to LIRMM Server

		
//		double support = 0.1; 

		GraankAlgo gr = new GraankAlgo();
		gr.run(support);


		long afterUsedMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long actualMemUsed = afterUsedMem - beforeUsedMem;
//		System.out.println("Used memory in bytes: " + actualMemUsed);
		
		
		

		
		System.out.println("\nNaive Approach 'totalMemory() - freeMemory()' used memory : " + actualMemUsed / megaByte);
		
		System.out.println("\n");
		
	    java.util.List<MemoryPoolMXBean> poolss = ManagementFactory.getMemoryPoolMXBeans();
	    long total = 0, total_non_heap =0;
	    for (MemoryPoolMXBean memoryPoolMXBean : poolss)
	    {
	      if (memoryPoolMXBean.getType() == MemoryType.HEAP)
	      {
	        long peakUsed = memoryPoolMXBean.getPeakUsage().getUsed();
	        System.out.println("Peak used for Heap: " + memoryPoolMXBean.getName() + " is: " + peakUsed);
	        total = total + peakUsed;
	      }
	      
	      if (memoryPoolMXBean.getType() == MemoryType.NON_HEAP)
	      {
	        long peakUsed_non_heap = memoryPoolMXBean.getPeakUsage().getUsed();
	        System.out.println("Peak used for Non_Heap: " + memoryPoolMXBean.getName() + " is: " + peakUsed_non_heap);
	        total_non_heap = total_non_heap + peakUsed_non_heap;
	      }
	      
	    }
	    System.out.println("\nTotal heap peak used: " + total/ megaByte+ " MB");
	    System.out.println("Total non_heap peak used: " + total_non_heap/ megaByte+ " MB");
	    System.out.println("Total heap peak used: " + (total+total_non_heap)/ megaByte + " MB");
	    
	    

		
	    System.out.println("\nTotal Heap Peak Used : " + (total+total_non_heap)/ megaByte);
	    
	
		end_time = System.nanoTime();

		elapsedTime = end_time - start_time;
		double seconds = (double) elapsedTime / 1000000000.0;
		System.out.println("Program Run time : " + seconds + " seconds");


		

	}

}


